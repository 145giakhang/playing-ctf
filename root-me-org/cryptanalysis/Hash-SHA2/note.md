Simple challenge. Some famous SHA-2 hash functions:
- SHA256: 32 bytes
- SHA384: 48 bytes
- SHA512: 64 bytes

However, our hash string has 65 characters => one character is redundant. If we skim through the hash string, we can find out character `k` is not in hexadecimal format. So delete it, and get this correct hash string: **96719db60d8e3f498c98d94155e1296aac105c4923290c89eeeb3ba26d3eef92** to [HashKiller](https://hashes.com/en/decrypt/hash) to decrypt

Simple script to detect wrong format character:
```python
s = "96719db60d8e3f498c98d94155e1296aac105ck4923290c89eeeb3ba26d3eef92"
possible_hex = [str(x) for x in range(10)] + ['a', 'b', 'c', 'd', 'e', 'f']

for i in range(len(s)):
    if s[i] not in possible_hex:
        print(s[i])    
        print(s[0:i] + s[i+1:])
```