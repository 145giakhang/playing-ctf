text = open('ch7.bin', 'rb').read()
for i in range(101):
    try:
        print("{}: {}".format(i, ''.join([chr(c - i) for c in text])))
    except Exception as err:
        print('Error: ' + str(err))
