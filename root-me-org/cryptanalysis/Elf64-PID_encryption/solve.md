# [root-me.org/Cryptanalysis] ELF64 - PID encryption

## Challenge
```
Host	    challenge01.root-me.org
Protocol	SSH
Port	    2221
SSH access	ssh -p 2221 cryptanalyse-ch21@challenge01.root-me.org     WebSSH
Username	cryptanalyse-ch21
Password	cryptanalyse-ch21
```

Source code:
```c
/*
 * gcc ch21.c -lcrypt -o ch21
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <crypt.h>
#include <sys/types.h>
#include <unistd.h>
 
int main (int argc, char *argv[]) {
    char pid[16];
    char *args[] = { "/bin/bash", "-p", 0 };
 
    snprintf(pid, sizeof(pid), "%i", getpid());
    if (argc != 2)
        return 0;
 
    printf("%s=%s",argv[1], crypt(pid, "$1$awesome"));
 
    if (strcmp(argv[1], crypt(pid, "$1$awesome")) == 0) {
        printf("WIN!\n");
        execve(args[0], &args[0], NULL);
 
    } else {
        printf("Fail... :/\n");
    }
    return 0;
}
```

## Code explain
The program needs an argument A. Then it takes the PID of itself and encrypts with a seed `$1$awesome`, compares to A. If equals, open a shell so that we can get the password, otherwise exit.

## Solution
We need a way to know the PID of the program before it executes, then we can calculate the encryption ourself and pass it to the program. We can use the `exec` family in C, for example `execve()`, which according to Linux man page:
> One sometimes sees execve() (and the related functions described in exec(3)) described as "executing a new process" (or similar).  This is a highly misleading description: there is no new process; many attributes of the calling process remain unchanged (in particular, its **PID**).  All that execve() does is arrange for an existing process (the calling process) to execute a new program 

SSH to the server, create a `control.c` in **/tmp**:
```c
/* gcc control.c -lcrypt -o control */
#include <stdio.h>
#include <string.h>
#include <crypt.h>
#include <unistd.h>

int main(void) {
	// get current PID
	char pid[16];
	snprintf(pid, sizeof(pid), "%i", getpid());
	// calculate the hash string
	char *s = crypt(pid, "$1$awesome");
	char *args[] = {"/challenge/cryptanalyse/ch21/ch21", s, 0};
	execve(args[0], &args[0], NULL);
	return 0;
}
```

Then run it:

![](./images/flag.png)