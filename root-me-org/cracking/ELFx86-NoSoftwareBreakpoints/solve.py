#!/usr/bin/python3

import sys

def getChecksum(dumpfile):
    try:
        fp = open(dumpfile, 'r')
        lst = fp.read().rstrip().split()
        # $ecx = 0
        ecx = '0' * 32
        for i in range(163):
            sum = int(ecx, 2) + int(lst[i], 16)
 
            # get $cl
            clstr = str(bin(sum))[2:]
            if len(clstr) < 8:
                clstr = '0'*(8-len(clstr)) + clstr
            elif len(clstr) > 8:
                clstr = clstr[-8:]
            
            # append $cl to $ecx
            ecx = ecx[:24] + clstr
            # rotate left 3
            ecx = ecx[3:] + ecx[:3]

        return list(ecx)
    except IOError:
        print("Cannot open file!")
        return None

if __name__ == '__main__':
    # checksum is calculated from .text section, then stored in $edx
    edx = getChecksum('textsection.txt')
    if edx:
        res = bytearray()
        hexstr = ''
        # $al is the constant secret key, get by debugging using gdb
        al_bytearr = bytearray.fromhex('e0324f2679da96af9997ac15deec9d356478fc8734d52acd1e')
        
        for i in range(25):
            # rotate right by 1
            edx = edx[-1:] + edx[:-1]
            # binary string to hex bytearray, truncate leading '0x',
            # and just take last 8 bits since only $dl is used,
            # note that append leading '0' for hex from 0x0->0xf
            h = hex(int(''.join(edx[-8:]), 2))[2:]
            if len(h) != 2:
                h = '0' + h
            hexstr += h
        
        # hex string to bytearray
        dl_bytearr = bytearray.fromhex(hexstr)
        res = [al_bytearr[i] ^ dl_bytearr[i] for i in range(25)]
        
        s = ''.join([chr(x) for x in res])
        print(s[::-1])
    else:
        sys.exit()

