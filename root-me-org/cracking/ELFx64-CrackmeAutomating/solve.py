import re

# open check.txt to parse mov and xor opcode instructions
with open("check.txt", "r") as cf:
    # regex pattern for the instructions
    mov_pattern = "movl   (\$0x)([0-9a-f]+),-0x8\(%rbp\)"
    xor_al_pattern = "xor    (\$0x)([0-9a-f]+),%al"
    xor_eax_pattern = "xor    (\$0x)([0-9a-f]+),%eax"
    line = cf.readline().strip()
    res = []
    while line:
        # mov to [rbp-0x8] instructions
        if re.search(mov_pattern, line):
            val = line.split()[4]
            res.append(val)
        # xor with $al instructions
        elif re.search(xor_al_pattern, line):
            last = int(res.pop(), 16)
            val = hex(int(line.split()[2], 16) ^ last)[2:]
            # if val = 0-9 => leading 0
            if len(val) == 1:
                val = '0' + val
            res.append(val)
        # xor with $eax instructions
        elif re.search(xor_eax_pattern, line):
            last = int(res.pop(), 16)
            val = hex(int(line.split()[3], 16) ^ last)[2:]
            # if val = 0-9 => leading 0
            if len(val) == 1:
                val = '0' + val
            res.append(val)

        line = cf.readline()

    # convert to ASCII character
    lst = [chr(int(e, 16)) for e in res]

    # write solution
    try:
        sf = open("solution", "w+")
        sf.write("".join(lst))
    except IOError:
        print("Error: something went wrong when open solution file")
    finally:
        sf.close()



