# [root-me.org] ELFx64 - Crackme Automating

## Test the program
```
$ ./ch30.bin
Crackme : Validate a base64 file
Usage: ./ch30.bin <file name>
```

Create a base64-encoded file:
```
$ touch myfile
/* MTIzNDU2Nzg5Cg== */
$ echo "123456789" | base64 > myfile        
$ ./ch30.bin myfile
Checking myfile
File is 17 bytes long.

	I'm sorry, Dave, I can't let you do this.
```

## Analyze the program
Examine "meaningful" strings:
```
$ strings ./ch30.bin
...
	I'm sorry, Dave, I can't let you do this.
	You did it!
	Check the file and figure next step.
Crackme : Validate a base64 file
Usage: %s <file name>
Checking %s
...
File is %d bytes long.
File %s is too big
Abandoning here. Sorry.
...
badboy
...
goodboy
```

**ltrace**:
```
$ ltrace ./ch30.bin myfile
printf("Checking %s\n", "myfile"Checking myfile
)                              = 16
fopen("myfile", "rb")                                          = 0x55707ffda670
fseek(0x55707ffda670, 0, 2, 0)                                 = 0
ftell(0x55707ffda670, 0x55707ffda750, 0, 17)                   = 17
fseek(0x55707ffda670, 0, 0, 0)                                 = 0
printf("File is %d bytes long.\n", 17File is 17 bytes long.
)                         = 23
fread(0x7ffd2ec4b7a0, 17, 1, 0x55707ffda670)                   = 1
fclose(0x55707ffda670)                                         = 0
puts("\n\tI'm sorry, Dave, I can't let y"...
	I'm sorry, Dave, I can't let you do this.

)                  = 45
exit(0 <no return ...>
+++ exited (status 0) +++
```

Not very informative!!!

**objdump**:
```
$ objdump -d ./ch30.bin > dump.txt
```
Notice a suspicious function `check`, which is very very long:
```
0000000000000ac5 <check>:
     ac5:	55                   	push   %rbp
     ac6:	48 89 e5             	mov    %rsp,%rbp
     ac9:	48 83 ec 20          	sub    $0x20,%rsp
     acd:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
     ad1:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
     ad8:	c7 45 f8 f7 00 00 00 	movl   $0xf7,-0x8(%rbp)
     adf:	8b 45 fc             	mov    -0x4(%rbp),%eax
     ae2:	48 63 d0             	movslq %eax,%rdx
     ae5:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     ae9:	48 01 d0             	add    %rdx,%rax
     aec:	0f b6 00             	movzbl (%rax),%eax
     aef:	0f be c0             	movsbl %al,%eax
     af2:	34 a3                	xor    $0xa3,%al
     af4:	3b 45 f8             	cmp    -0x8(%rbp),%eax
     af7:	74 0a                	je     b03 <check+0x3e>
     af9:	8b 45 fc             	mov    -0x4(%rbp),%eax
     afc:	89 c7                	mov    %eax,%edi
     afe:	e8 ed fd ff ff       	callq  8f0 <badboy>
     b03:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
... (to be continued)
```

This function does some comparisons, if equal then jump to somewhere else, else call **badboy**. We need more information.

## Debugging with GDB

Set breakpoint at **check** function:

```
$ gdb -q ./ch30.bin
Reading symbols from ./ch30.bin...(no debugging symbols found)...done.
gdb-peda$ b check
Breakpoint 1 at 0xac9
gdb-peda$ r myfile
``` 
The program stores a value `0xf7` into **[rbp-0x8]**, stores our input (MTIzNDU2Nzg5Cg==) into **rax**, get the first character by adding **rdx** = 0 which is counter:
```
   0x555555554ad1 <check+12>:	mov    DWORD PTR [rbp-0x4],0x0
=> 0x555555554ad8 <check+19>:	mov    DWORD PTR [rbp-0x8],0xf7
   0x555555554adf <check+26>:	mov    eax,DWORD PTR [rbp-0x4]
   0x555555554ae2 <check+29>:	movsxd rdx,eax
   0x555555554ae5 <check+32>:	mov    rax,QWORD PTR [rbp-0x18]
   0x555555554ae9 <check+36>:	add    rax,rdx
```
Then XOR the character to a constant `0xa3`, compare with **[rbp-0x8]** which is `0xf7`. If equals then jump to another block, else call **badboy**. And there are a lot, I mean, thoundsands of blocks just do the same thing, but with different constants. 
```
   0x555555554af2 <check+45>:	xor    al,0xa3
=> 0x555555554af4 <check+47>:	cmp    eax,DWORD PTR [rbp-0x8]
   0x555555554af7 <check+50>:	je     0x555555554b03 <check+62>
   0x555555554af9 <check+52>:	mov    eax,DWORD PTR [rbp-0x4]
   0x555555554afc <check+55>:	mov    edi,eax
   0x555555554afe <check+57>:	call   0x5555555548f0 <badboy>
```

So our goal is clear: find 2 constants in each block, XOR together to get the right input. But we're facing 3 problems here:
- First, there are too many of blocks like the above block, we cannot do it manually => write some scripts to do it for us.
- Second, with the XOR instructions, in some case it is "_**xor** with $al_" and in another case is "_**xor** with $eax_". Notice this, since it took me 1.5 day to figure out where I did wrong.
- Finally, at some point, there is no XOR operation at all. The program just takes our input and compares directly to a constant. Dig into the **check** function and you may find out.

## Code some scripts
The script is to parse the hex dump of the **check** function, take out the 2 constants (in case has XOR operation) or just 1 constants (in case compare directly).

Get the hexdump of the **check** function, save in `check.txt`. You can do anyway to get it done. In my case, I re-use the `dump.txt` above, open with VIM (*magical VIM* :v), in normal mode:
```
:<begin-line-number>,<end-line-number>y<Enter>
```

Now the script:
```
import re

# open check.txt to parse mov and xor opcode instructions
with open("check.txt", "r") as cf:
    # regex pattern for the instructions
    mov_pattern = "movl   (\$0x)([0-9a-f]+),-0x8\(%rbp\)"
    xor_al_pattern = "xor    (\$0x)([0-9a-f]+),%al"
    xor_eax_pattern = "xor    (\$0x)([0-9a-f]+),%eax"
    line = cf.readline().strip()
    res = []
    while line:
        # mov to [rbp-0x8] instructions
        if re.search(mov_pattern, line):
            val = line.split()[4]
            res.append(val)
        # xor with $al instructions
        elif re.search(xor_al_pattern, line):
            last = int(res.pop(), 16)
            val = hex(int(line.split()[2], 16) ^ last)[2:]
            # if val = 0-9 => leading 0
            if len(val) == 1:
                val = '0' + val
            res.append(val)
        # xor with $eax instructions
        elif re.search(xor_eax_pattern, line):
            last = int(res.pop(), 16)
            val = hex(int(line.split()[3], 16) ^ last)[2:]
            # if val = 0-9 => leading 0
            if len(val) == 1:
                val = '0' + val
            res.append(val)

        line = cf.readline()

    # convert to ASCII character
    lst = [chr(int(e, 16)) for e in res]

    # write solution
    try:
        sf = open("solution", "w+")
        sf.write("".join(lst))
    except IOError:
        print("Error: something went wrong when open solution file")
    finally:
        sf.close()
```

The code is quite clear so no need to explain guys!!! Here is the result after run this script, a base64-encoded string:
```
$ cat solution
... (too much up here)
c0dldFZhbHVlQDQAX19pbXBfX0RlbGV0ZUNyaXRpY2FsU2VjdGlvbkA0AF9MZWF2ZUNyaXRpY2Fs
U2VjdGlvbkA0AF9fUlVOVElNRV9QU0VVRE9fUkVMT0NfTElTVF9FTkRfXwBfX2xpYmtlcm5lbDMy
X2FfaW5hbWUAX19fZHluX3Rsc19pbml0X2NhbGxiYWNrAF9fdGxzX3VzZWQAX19fY3J0X3h0X2Vu
ZF9fAF92ZnByaW50ZgBfX2ltcF9fRW50ZXJDcml0aWNhbFNlY3Rpb25ANABfX2ltcF9fZndyaXRl
AA==
```
Verify it:
```
$ ./ch30.bin solution
Checking solution
File is 86553 bytes long.

	You did it!
	Check the file and figure next step.
```

Decode it:
```
$ base64 --decode solution > decoded
```
Here is `decoded` file:
![root-me.org](./decoded.png)

It is a PE executable object file (.EXE) with **MZ** signature.
```
$ file decoded
decoded: PE32 executable (console) Intel 80386, for MS Windows
```

Open it with OllyDBG in Windows system, we can see that it is exactly the same as the ELF executable file **ch30.bin**, just for Windows OS. Or I just dump it:
```
$ objdump -d decoded > dump2.txt
``` 
And get hexdump of the **_check** function, just like above, and save in `check2.txt`
```
00401514 <_check>:
  401514:	55                   	push   %ebp
  401515:	89 e5                	mov    %esp,%ebp
  401517:	83 ec 28             	sub    $0x28,%esp
  40151a:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  401521:	c7 45 f0 9e 00 00 00 	movl   $0x9e,-0x10(%ebp)
  401528:	8b 55 f4             	mov    -0xc(%ebp),%edx
  40152b:	8b 45 08             	mov    0x8(%ebp),%eax
  40152e:	01 d0                	add    %edx,%eax
  401530:	0f b6 00             	movzbl (%eax),%eax
  401533:	0f be c0             	movsbl %al,%eax
  401536:	34 d6                	xor    $0xd6,%al
  401538:	3b 45 f0             	cmp    -0x10(%ebp),%eax
  40153b:	74 0b                	je     401548 <_check+0x34>
  40153d:	8b 45 f4             	mov    -0xc(%ebp),%eax
  401540:	89 04 24             	mov    %eax,(%esp)
  401543:	e8 f8 fd ff ff       	call   401340 <_badboy>
... (to be continued)
```
Now modify our script to run again this new file as in `solve2.py`. Then we get the "real" solution:
```
$ cat solution2
Hey this is the final steps.
Go further, don't give up!

Heishiro Mitsurugi is one of the most recognizable characters in the Soul series of fighting games. Mitsurugi made his first appearance in Soul Edge and has returned for all six sequels: Soulcalibur, Soulcalibur II, Soulcalibur III, Soulcalibur IV, Soulcalibur: Broken Destiny and Soulcalibur V. He also appears as a playable character in Soulcalibur Legends and Soulcalibur: Lost Swords, as He Who Lives for Battle.

All I need here is a long text, just because I want you to be able to reverse it. I hope you'll learn some good things. Automatizing things can be really good.

The flag for this challenge is "I_reverse_all_this_and_all_I_got_is_this_flag" without the quotes.
```
Phewww! A very tricky challenge, I learnt a lot from it. The way to get the flag is obvious, but the interesting part is how to do it. Have fun :v !
