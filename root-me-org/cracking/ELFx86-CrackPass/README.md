# [root-me.org/Cracking] ELF x86 - Crack Pass

Check file info:
```
$ file Crack
Crack: ELF 32-bit LSB executable, Intel 80386, version 1 (SYSV), dynamically linked, interpreter /lib/ld-, for GNU/Linux 2.6.8, stripped
```

Notice the **ptrace** call:
```
$ readelf --syms --dyn-syms Crack
Symbol table '.dynsym' contains 11 entries:
   Num:    Value  Size Type    Bind   Vis      Ndx Name
     0: 00000000     0 NOTYPE  LOCAL  DEFAULT  UND
     1: 00000000   583 FUNC    GLOBAL DEFAULT  UND abort@GLIBC_2.0 (2)
     2: 00000000    49 FUNC    GLOBAL DEFAULT  UND sprintf@GLIBC_2.0 (2)
     3: 00000000     0 NOTYPE  WEAK   DEFAULT  UND __gmon_start__
     4: 00000000   129 FUNC    GLOBAL DEFAULT  UND ptrace@GLIBC_2.0 (2)
     5: 00000000   441 FUNC    GLOBAL DEFAULT  UND __libc_start_main@GLIBC_2.0 (2)
     6: 00000000    60 FUNC    GLOBAL DEFAULT  UND __ctype_b_loc@GLIBC_2.3 (3)
     7: 00000000    54 FUNC    GLOBAL DEFAULT  UND printf@GLIBC_2.0 (2)
     8: 00000000   460 FUNC    GLOBAL DEFAULT  UND puts@GLIBC_2.0 (2)
     9: 00000000    67 FUNC    GLOBAL DEFAULT  UND strcmp@GLIBC_2.0 (2)
    10: 0804879c     4 OBJECT  GLOBAL DEFAULT   15 _IO_stdin_used
```
Dump file and where the **ptrace** is called:
```
$ objdump -d Crack > dump.txt
$ vim dump.txt

804867e:   c7 04 24 00 00 00 00    movl   $0x0,(%esp)
8048685:   e8 52 fd ff ff          call   80483dc <ptrace@plt>                                   
804868a:   85 c0                   test   %eax,%eax
```
So we knew that the **ptrace** function is called at *0x8048685*

Open with **GDB**:
```
$ gdb ./Crack
(gdb) info file
Symbols from "/media/khang/DATA1/playing-ctf/RE/root-me-org/cracking/ELFx86-CrackPass/Crack".
Local exec file:
        `/media/khang/DATA1/playing-ctf/RE/root-me-org/cracking/ELFx86-CrackPass/Crack', file type elf32-i386.
        Entry point: 0x8048440
        ...
```
Set breakpoint at entry point:
```
(gdb) b *0x8048440
(gdb) r 123456
```
Set breakpoint at *0x8048685* (call ptrace):
```
(gdb) b *0x8048685
(gdb) c

   0x8048685:   call   0x80483dc <ptrace@plt>
   0x804868a:   test   eax,eax
=> 0x804868c:   jns    0x804869f
```
**ptrace** is an anti-debug technique. The program will continue if SF = 0, else exit. So we need to toggle the SF flag to 0 to by-pass the trick:
```
(gdb) set $eflags = $eflags ^ 0x80
(gdb) s
```

Find in dump file we can see a call to **strcmp** function at *0x8048617*, set breakpoint there:
```
(gdb) b *0x8048617
(gdb) c

[-------------------------------------code-------------------------------------]
   0x804860b:   call   0x804851c
   0x8048610:   mov    DWORD PTR [esp+0x4],esi
   0x8048614:   mov    DWORD PTR [esp],ebx
=> 0x8048617:   call   0x804842c <strcmp@plt>
   0x804861c:   test   eax,eax
   0x804861e:   jne    0x8048632
   0x8048620:   mov    DWORD PTR [esp+0x4],ebx
   0x8048624:   mov    DWORD PTR [esp],0x80487e8
Guessed arguments:
arg[0]: 0xffffbe90 ("ff07031d6fb052490149f44b1d5e94f1592b6bac93c06ca9")
arg[1]: 0xffffbf10 ("123456")
```

We can see that it takes 2 arguments: our input string and a long hex string. Try again:
```
$ ./Crack ff07031d6fb052490149f44b1d5e94f1592b6bac93c06ca9
Good work, the password is : 

ff07031d6fb052490149f44b1d5e94f1592b6bac93c06ca9
```
