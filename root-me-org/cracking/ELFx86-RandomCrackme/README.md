# [root-me.org/Cracking] ELF x86 - Random Crackme

## Test the executable
```
$ file crackme_wtf
crackme_wtf: current ar archive
$ ar xo crackme_wtf
$ file Crackme
Crackme: ELF 32-bit LSB executable, Intel 80386, version 1 (SYSV), dynamically linked, interpreter /lib/ld-, for GNU/Linux 2.6.8, with debug_info, not stripped
$ ./Crackme
    ** Bienvenue dans ce challenge de cracking **    

[+] Password :123456

[!]Access Denied !  
[-] Try again
```

## Examine the executable

```
$ strings ./Crackme
...
_VQLGE_TQPTYD_KJTIV_
...
[-]Debugger detecte ... Exit
...
[+]Good password                                   
[+] Clee de validation du crack-me :
...
ptrace@@GLIBC_2.0
...
```
We can see that **ptrace** is used as an anti-debug technique in this executable. Besides, the string *"Good password"* is given as a starting point for us. And maybe after passing the password validation, a serial key will be popped out with the string *"Clee de validation du crack-me :"*. Finally, *"_VQLGE_TQPTYD_KJTIV_"* is just a mystique, key-like string.

Although it has **ptrace** anti-debug technique, I still want to test it againts **ltrace**:

```
$ ltrace ./Crackme
...
sprintf("153726315_VQLGE_TQPTYD_KJTIV_", "%lld%s", 153726315, nil) = 29
...
ptrace(0, 0, 1, 0)                               = 0xffffffff
fwrite("[-]Debugger detecte ... Exit\n", 1, 29, 0xf7eb2ce0[-]Debugger detecte ... Exit
) = 29
exit(1 <no return ...>
+++ exited (status 1) +++

$ ltrace ./Crackme
sprintf("317370744_VQLGE_TQPTYD_KJTIV_", "%lld%s", 317370744, nil) = 29
...
ptrace(0, 0, 1, 0)                               = 0xffffffff
fwrite("[-]Debugger detecte ... Exit\n", 1, 29, 0xf7ef1ce0[-]Debugger detecte ... Exit
) = 29
exit(1 <no return ...>
+++ exited (status 1) +++
```

Each time we run the program, a random number is generated and concatenated to the string *"_VQLGE_TQPTYD_KJTIV_"*. For what???

## Bypass the ptrace anti-debug

Dump file and look for where the **ptrace** is called:

```
$ objdump -d ./Crackme > dump.txt
$ vim dump.txt
...
8048af2:	e8 c9 fb ff ff       	call   80486c0 <ptrace@plt>
...
```
Open with **gdb-peda**:
```
$ gdb ./Crackme
(gdb-peda) b *0x8048af2
(gdb-peda) r
[----------------------------code-------------------------]
   0x8048adb <main+599>:        mov    DWORD PTR [esp+0x8],0x1
   0x8048ae3 <main+607>:        mov    DWORD PTR [esp+0x4],0x0
   0x8048aeb <main+615>:        mov    DWORD PTR [esp],0x0
=> 0x8048af2 <main+622>:        call   0x80486c0 <ptrace@plt>
   0x8048af7 <main+627>:        test   eax,eax
   0x8048af9 <main+629>:        jns    0x8048b2c <main+680>
   0x8048afb <main+631>:        mov    eax,ds:0x804b064
   0x8048b00 <main+636>:        mov    DWORD PTR [esp+0xc],eax
Guessed arguments:
arg[0]: 0x0 
arg[1]: 0x0 
arg[2]: 0x1 
arg[3]: 0x0 
```
After **ptrace** call, SF = 1 --> jump will not taken --> lead to badboy. So we need to toggle SF flag to 0 to bypass it, and make the jump happen.

```
(gdb-peda) eflags toggle sign
(gdb-peda) n
```

After let us input the password, the program will call function **clean** which take our input as argument:
```
[----------------------------code------------------------]
   0x8048b5b <main+727>:        call   0x80486e0 <fgets@plt>
   0x8048b60 <main+732>:        lea    eax,[ebp-0x7c]
   0x8048b63 <main+735>:        mov    DWORD PTR [esp],eax
=> 0x8048b66 <main+738>:        call   0x8048e4c <clean>
   0x8048b6b <main+743>:        mov    DWORD PTR [esp],0x54
   0x8048b72 <main+750>:        call   0x8048760 <malloc@plt>
   0x8048b77 <main+755>:        mov    DWORD PTR [ebp-0x10094],eax
   0x8048b7d <main+761>:        cmp    DWORD PTR [ebp-0x10094],0x0
Guessed arguments:
arg[0]: 0xffffce9c ("123456\n")
```
Function **clean** calls C library function **strchr**, which takes our input *"123456"* and newline character as arguments. The **strchr** returns a pointer to the first occurrence of newline character in our input string, then compares it with terminating null-character *'\0'*. If equals jump to 0x8048e7d, else exit **clean**.
```
[----------------------------code------------------------]
   0x8048e52 <clean+6>: mov    DWORD PTR [esp+0x4],0xa
   0x8048e5a <clean+14>:        mov    eax,DWORD PTR [ebp+0x8]
   0x8048e5d <clean+17>:        mov    DWORD PTR [esp],eax
=> 0x8048e60 <clean+20>:        call   0x80486d0 <strchr@plt>
   0x8048e65 <clean+25>:        mov    DWORD PTR [ebp-0x4],eax
   0x8048e68 <clean+28>:        mov    DWORD PTR [ebp-0x8],0x0
   0x8048e6f <clean+35>:        cmp    DWORD PTR [ebp-0x4],0x0
   0x8048e73 <clean+39>:        je     0x8048e7d <clean+49>
Guessed arguments:
arg[0]: 0xffffce9c ("123456\n")
arg[1]: 0xa ('\n')
```
Obviously they are not equal, so I just toggled the ZF flag:
```
(gdb-peda) eflags toggle zero
(gdb-peda) n
[----------------------------code------------------------]
   0x8048e75 <clean+41>:        mov    eax,DWORD PTR [ebp-0x4]
   0x8048e78 <clean+44>:        mov    BYTE PTR [eax],0x0
   0x8048e7b <clean+47>:        jmp    0x8048e91 <clean+69>
=> 0x8048e7d <clean+49>:        call   0x80486a0 <getchar@plt>
   0x8048e82 <clean+54>:        mov    DWORD PTR [ebp-0x8],eax
   0x8048e85 <clean+57>:        cmp    DWORD PTR [ebp-0x8],0xa
   0x8048e89 <clean+61>:        je     0x8048e91 <clean+69>
   0x8048e8b <clean+63>:        cmp    DWORD PTR [ebp-0x8],0xffffffff
No argument
```
The program then gets next input character from **stdin** (from us :v), and compare with newline character *'\n'*. If equals, exit **clean**