# [root-me.org] ELFx86 - AntiDebug

## Test the proram

```
$ ./ch13
Enter the password: 123456
Bad password, sorry !
```

## Analyze the program

Open the program in IDA, we can see a bunch of *sys_signal* and jump instruction. In the **Function window**, only 2 functions are visible: **start** and **sub_80480E2**, which is called right after the program's beginning:

```
08048063
08048063 loc_8048063:
08048063 mov     eax, 30h
08048068 mov     ebx, 5
0804806D mov     ecx, offset sub_80480E2
08048072 int     80h             ; LINUX - sys_signal
08048074 jmp     short loc_8048077
```
Jump to **sub_80480E2**:

- First, store value `8048104` to EAX:
```
080480E2 sub_80480E2 proc near
080480E2 mov     eax, offset dword_8048104
080480E7 jmp     short loc_8048101
```

- Second, compare EAX to `80482E8`, which is the end of the program (by examining the hex dump of executable file). If equal then return, else XOR the value of EAX with a constant `8048FC1`, and loop again:
```
080480E9 loc_80480E9:
080480E9 cmp     eax, 80482E8h
080480EE jz      short locret_8048103
```

So we know that the task of this function is self-modifying the code, by XORing each double word from `0x8048104` to `0x80482E8`. Open executable file in **HxD** hex editor, and here is the binary data will be modified (stored in a file named `neededit.txt`):
```
7A 8E 04 08 C1 36 39 8A C5 87 BE 1C C1 8F 04 E0 74 8F 04 08 7A 8F 04 08 C1 36 55 8A C5 87 BE 88 C1 8F 04 E0 68 8F 04 08 7A DE 86 0C C9 8E C7 43 07 8C 04 CB 79 DE 86 0C C9 0F 3C 08 B5 89 84 38 3D CF EF FD 02 37 55 8A C5 87 BF D9 43 8B 0C 82 C9 B5 0F 7D C8 0F FD 08 B5 92 44 4B 2A 7E BF 09 C1 8F 04 B1 C6 0D 00 00 7B 99 04 08 C1 67 53 08 C1 8F EC 6A C1 8F 04 B3 C0 8F 04 08 78 6B 85 0C C9 35 27 08 C1 8F EC 36 C1 8F 04 E0 88 8F 04 08 F0 4F BF 68 41 8B 0C 89 3A 6B 86 0C C9 FB 03 3B C2 0C C7 0C 2A 7E BF EC 43 8B 0C 33 C2 FB 1D B3 C0 8F 04 08 78 92 86 0C C9 35 24 08 C1 8F EC 0E C1 8F 04 E0 D0 8F 04 08 02 37 00 08 C1 8F C9 88 02 37 07 08 C1 8F C9 88 02 37 05 08 C1 8F C9 88 86 FD 65 7C BB A3 24 7C A9 E6 77 28 A8 FC 24 7C A9 EA 24 6F AE E0 60 28 B1 EE 77 7B B6 E0 76 6C E1 AE 0E 4A A0 EB 24 78 A0 FC 77 7F AE FD 60 24 E1 FC 6B 7A B3 F6 24 29 CB DE 51 41 95 AF 25 28 84 F7 61 6B B4 FB 65 6A AD EA 24 61 B2 AF 69 67 A5 E6 62 61 A4 EB 24 29 CB CA 6A 7C A4 FD 24 7C A9 EA 24 78 A0 FC 77 7F AE FD 60 32 E1 8F 04 08 C1 8F 04 08 C1 8F 04 08 C1 8F 04 08 C1 8F 04 08 C1 8F 04 08 C1 8F 04 08 C1 8F 04 08 C1 8F 04 08 C1 8F 04 08 C1 8F 04 08 C1 8F 04 08 C1 8F 04 08 C1 8F 04 08 C1 8F 04 08 C1 8F 04 08 C1 8F 04 08 C1 8F 04 08 C1 8F 04 08 C1 8F 04 08 C1 8F 04 08 C1 8F 04 08 C1 8F 04 08 C1 8F 04 08 C1 8F 04 08 C1 8F 04 08 C1 8F 04 08 C1 8F 04 08 C1 8F 04 08 C1 8F 04 08 C1 8F 04 08 C1 8F 04 08 C1 2A CB 95 75 52 8C BC 54 20 91 A7 49 3B CB 9F 78 0A D9 08 CB E0 5E 76
```

I wrote a script `script.py` to modify it. One thing to remember that the binary above is *little endian*:
```
with open("neededit.txt", "r") as f:
    # single-byte array
    b_arr = f.read().rstrip().split()
    const = [0xc1, 0x8f, 0x04, 0x08]
    i = 0
    while i <= (len(b_arr)-4):
        for j in range(i, i+4):
            b_arr[j] = hex(int(b_arr[j], 16) ^ const[j % 4])[2:]
            # if only one HEX char => leading zero
            if len(b_arr[j]) == 1:
                b_arr[j] = '0' + b_arr[j]

        i += 4

    res = " ".join(b_arr)
    print(res)
```
Run it:
```
$ python3 script.py
bb 01 00 00 00 b9 3d 82 04 08 ba 14 00 00 00 e8 b5 00 00 00 bb 00 00 00 00 b9 51 82 04 08 ba 80 00 00 00 e8 a9 00 00 00 bb 51 82 04 08 01 c3 4b c6 03 00 c3 b8 51 82 04 08 80 38 00 74 06 80 30 fc 40 eb f5 c3 b8 51 82 04 08 bb d1 82 04 08 8a 08 3a 0b 75 09 80 f9 00 74 1d 40 43 eb f1 bb 01 00 00 00 b9 07 82 04 08 ba 16 00 00 00 e8 57 00 00 00 e8 62 00 00 00 bb 01 00 00 00 b9 e4 81 04 08 ba 23 00 00 00 e8 3e 00 00 00 e8 49 00 00 00 31 c0 bb 60 80 04 08 81 fb e4 82 04 08 74 07 33 03 83 c3 04 eb f1 bb e4 82 04 08 3b 03 74 19 bb 01 00 00 00 b9 1d 82 04 08 ba 20 00 00 00 e8 06 00 00 00 e8 11 00 00 00 c3 b8 04 00 00 00 cd 80 c3 b8 03 00 00 00 cd 80 c3 b8 01 00 00 00 cd 80 47 72 61 74 7a 2c 20 74 68 69 73 20 69 73 20 74 68 65 20 67 6f 6f 64 20 70 61 73 73 77 6f 72 64 20 21 0a 42 61 64 20 70 61 73 73 77 6f 72 64 2c 20 73 6f 72 72 79 20 21 0a 51 55 49 54 20 21 20 45 78 65 63 75 74 61 62 6c 65 20 69 73 20 6d 6f 64 69 66 69 65 64 20 21 0a 45 6e 74 65 72 20 74 68 65 20 70 61 73 73 77 6f 72 64 3a 20 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 a5 cf 9d b4 dd 88 b4 95 af 95 af 88 b4 cf 97 b9 85 dd 00 0a 6f 5a 7e
```

Paste the binary above to the executable (also using **HxD** editor), and save as `ch13-edited`. Open in IDA, we see that there are more visible functions in **Functions window**. 

### sub_8048104
Call to **sys_write** (**sub_80481CD**) to print out the string *"Enter the password: "*
```
08048104 mov     ebx, 1
08048109 mov     ecx, offset aEnterThePasswo ; "Enter the password: "
0804810E mov     edx, 14h
08048113 call    sub_80481CD
```

Call to **sys_read** (**sub_80481D5**) to read input from user and store into `8048251`
```
08048118 mov     ebx, 0
0804811D mov     ecx, 8048251h
08048122 mov     edx, 80h
08048127 call    sub_80481D5
```

### sub_8048138
With each byte from the user input, XOR with constant `0xFC`:
```
08048138                 mov     eax, 8048251h
0804813D
0804813D loc_804813D:                            
0804813D                 cmp     byte ptr [eax], 0
08048140                 jz      short locret_8048148
08048142                 xor     byte ptr [eax], 0FCh
08048145                 inc     eax
08048146                 jmp     short loc_804813D
08048148 ; ---------------------------------------------------------------------------
08048148
08048148 locret_8048148:                         
08048148                 retn
08048148 sub_8048138     endp
```

### sub_8048149
Compare the XORed input to data at `80482D1`:
```
08048149                 mov     eax, 8048251h
0804814E                 mov     ebx, offset byte_80482D1
08048153
08048153 loc_8048153:                            
08048153                 mov     cl, [eax]
08048155                 cmp     cl, [ebx]
08048157                 jnz     short loc_8048162
08048159                 cmp     cl, 0
0804815C                 jz      short loc_804817B
0804815E                 inc     eax
0804815F                 inc     ebx
08048160                 jmp     short loc_8048153
```
Here is data at `80482D1`:
```
080482D1 byte_80482D1    db 0A5h, 0CFh, 9Dh      
080482D4                 dd 0B488DDB4h, 0AF95AF95h, 97CFB488h, 0DD85B9h
```
It means this array: `A5 CF 9D B4 DD 88 B4 95 AF 95 AF 88 B4 CF 97 B9 85 DD`. So here is the script `solve.py` to get the right input:
```
key = [0xa5, 0xcf, 0x9d, 0xb4, 0xdd, 0x88, 0xb4, 0x95, 0xaf, 0x95, 0xaf, 0x88, 0xb4, 0xcf, 0x97, 0xb9, 0x85, 0xdd]
const = 0xfc

res = "".join([chr(k ^ const) for k in key])
print("".join(res))
```
Run it:
```
$ python3 solve.py
Y3aH!tHiSiStH3kEy!
```