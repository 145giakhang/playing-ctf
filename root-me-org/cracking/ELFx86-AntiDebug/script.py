with open("neededit.txt", "r") as f:
    # single-byte array
    b_arr = f.read().rstrip().split()
    #const = [0x08, 0x04, 0x8f, 0xc1]
    const = [0xc1, 0x8f, 0x04, 0x08]
    i = 0
    while i <= (len(b_arr)-4):
        for j in range(i, i+4):
            b_arr[j] = hex(int(b_arr[j], 16) ^ const[j % 4])[2:]
            # if only one HEX char => leading zero
            if len(b_arr[j]) == 1:
                b_arr[j] = '0' + b_arr[j]

        i += 4

    res = " ".join(b_arr)
    print(res)
