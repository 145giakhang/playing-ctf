#!/bin/bash

j=0
for i in $(objdump -d readfile.o | grep "^ " | cut -f2); 
do 
    echo -n '\x'$i; 
    ((j++)); 
done;
printf "\nLen: $j\n"
