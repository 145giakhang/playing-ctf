#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/* gcc -m64 bof.c -o bof -z execstack -fno-stack-protector */
int main(int argc, char **argv) {
    char buffer[256];

    if(argc != 2) {
        exit(0);
    }

    printf("%p\n", buffer);
    strcpy(buffer, argv[1]);
    printf("%s\n", buffer);
    return 0;
}