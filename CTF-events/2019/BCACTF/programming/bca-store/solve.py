import math

def calcPriceChange(priceChange, items, paid):
    itemCount = {}
    for item in items:
        if item not in itemCount:
            itemCount[item] = 1
        else:
            itemCount[item] += 1
    price = 0
    # calculate A
    if 'A' in itemCount:
        price = itemCount['A'] * 45
    # calculate B
    if 'B' in itemCount:
        if itemCount['B'] % 2 == 0:
            price = price + (itemCount['B']/2) * 52 + (itemCount['B']/2) * 52 * 0.9
        else:
            price = price + ((itemCount['B']-1)/2 + 1) * 52 + ((itemCount['B']-1)/2) * 52 * 0.9
    # calculate C
    if 'C' in itemCount:
        if itemCount['C'] % 2 == 0:
            price = price + (itemCount['C']/2) * 67 + (itemCount['C']/2) * 67 * 0.5
        else:
            price = price + ((itemCount['C']-1)/2 + 1) * 67 + ((itemCount['C']-1)/2) * 67 * 0.5
    # calculate D
    if 'D' in itemCount:
        if itemCount['D'] % 3 == 0:
            price = price + (itemCount['D']//3) * 2 * 75
        else:
            remainder = itemCount['D'] % 3
            price = price + ((itemCount['D'] - remainder)//3) * 2 * 75 + remainder * 75
    
    if paid < price:
        priceChange.append(-1)
    else:
        priceChange.append(math.ceil((paid-price) * 100.0) / 100.0)
        #priceChange.append(paid - price)
    
    
try:
    fp = open('input.txt', 'r')
    # read inputs
    lines = fp.readlines()
    priceChange = []
    # loop 
    for i in lines:
        line = i[:-1]
        # split by spaces
        items = line.split()
        paid = int(items[-1])
        items = items[:-1]
        #print(paid, items)
        calcPriceChange(priceChange, items, paid)
        
    #print(" ".join(priceChange))
    for p in priceChange:
        print(p, end=' ')
finally:
    fp.close()