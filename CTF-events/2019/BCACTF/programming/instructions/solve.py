# read file
try:  
    fp = open('flag.txt', 'r')
    # do stuff here
    lineLst = fp.readlines()
    # array of numbers
    numLst = [int(x) for x in lineLst[0].split()]
    # search for viable lines
    characters = []
    idx = 0
    for obj in lineLst[1:]:
        # remove new line
        line = obj[:-1]
        if ((len(line) % 3 == 0) and (not '&' in line)):
            # counting from 1 => minus 1 for counting from 0
            characters.append(line[numLst[idx]-1])
            idx += 1

    print("".join(characters))
finally:  
    fp.close()