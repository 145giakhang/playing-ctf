require_relative 'params'

def to_complex(x)
  a, b = x
  w = Math::E ** Complex(0, Math::PI*2.0/3.0)
  a + b*w
end

def sub(x, y)
  a, b = x
  c, d = y
  [a-c, b-d]
end

def mul(x, y)
  a, b = x
  c, d = y
  [a*c - b*d, a*d + b*c - b*d]
end

def div(x, y)
  xc, yc = to_complex(x), to_complex(y)
  puts yc
  a, b = (xc / yc).rect
  #puts b
  [(a + b/Math.sqrt(3)).round, (b*2.0/Math.sqrt(3)).round]
end

def mod(x, y)
  # many times...
  100.times do
    k = div(x, y)
    x = sub(x, mul(k, y))
  end
  x
end

def norm(x)
  a, b = x
  a*a + b*b - a*b
end

flag = "TSGCTF{XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX}"

# # part I of flag TSGCTF{XXX...X = 36 => XXX...X = 29
# puts flag[0,flag.size/2]+" "+flag[0,flag.size/2].length.to_s
# # part II of flag XXX...X} = 37 => XXX...X = 36
# puts flag[flag.size/2,flag.size]+" "+flag[flag.size/2,flag.size].length.to_s
# # ==> total length of XXX...X = 29+36 = 65


# puts flag[0,flag.size/2].unpack("H*")[0].hex

# unpack each part of flag into MSB hex
msg  = [flag[0,flag.size/2], flag[flag.size/2,flag.size]].map {|text| text.unpack("H*")[0].hex}

modulos = MODULOS                 # assign that modulos = MODULOS from params.rb
=begin
calculate res
for each m (format [a,b]) in modulos, m = [mod(msg,[a,b]), [a,b]]
=end
res = modulos.map do |m|         
  [mod(msg, m), m]              
end

#puts msg
#=begin
puts 'MODULOS = %p' % [modulos] 
puts 'PROBLEM = %p' % [res]       # print res
#=end

# def reverseMod(xy, zt):
#   100.times do

# end

=begin
problem = [[[x,y], [z,t]]] where [z,t] comes from MODULOS
msg = [flag1, flag2]
[x,y] = mod(msg, [z,t]) (in calculate res)
To calculate msg => do in reversed order
=end
# problem = PROBLEM
# const1 = Math::E ** Complex(0, Math::PI*2.0/3.0)
# zi, ti = modulos[0]
# const2 = zi + ti*const1
# def calc(xy)
# flag1 = 
# 100.times do
