# [YauzaCTF - 2019] Crypto challenge: **enJoy!**

## Task
![enJoyChallenge](images/enjoychall.png)

## Solution
The challenge gives us a ***.rtf*** (Rich Text Format) file. I don't know much about this file type. First open this file with Word Editor, it looks like this:
![enJoyChallenge](images/enjoyrtf.png)

This [site](https://www.linuxquestions.org/questions/linux-newbie-8/opening-rtf-files-350295/) suggests convert to HTML file and view with browser.
So I did, and I got a Base64 encrypted text (**Note:** the encrypted text has been shortened for readable):

![enJoyChallenge](images/firstbase64.png)

Decode above encrypted text with [Base64 decoder](https://www.base64decode.org/), I got the second Base64 encrypted text (**Note:** the encrypted text has been shortened for readable):

![enJoyChallenge](images/secondbas64.png)

Decode again, now I got a string of hexadecimal (**Note:** the text has been shortened for readable):

![enJoyChallenge](images/hex.png)

Using [Hex to Text converter](http://www.convertstring.com/vi/EncodeDecode/HexDecode), I got a substitution ciphertext:

![enJoyChallenge](images/subscipher.png)

Using [Substitution solver](https://www.guballa.de/substitution-solver) to decrypt it:

![enJoyChallenge](images/plaintext.png)

So, **Tupper's self-referential formula** right? This formular visually represents itself when graphed at a specific location in the (x, y) plane. From [here](https://shreevatsa.wordpress.com/2011/04/12/how-does-tuppers-self-referential-formula-work/), we got the formula:

![enJoyChallenge](images/formula.png)

Maybe the very long number which represented in binary string we have received from the plaintext is the given number N for the formula. Then I found this [site](http://keelyhill.github.io/tuppers-formula/) which will draw the graph according to given N using Tupper's self-referential formula.
Using the given binary string, here is the flag:

![enJoyChallenge](images/flag.png)