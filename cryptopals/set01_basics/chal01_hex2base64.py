import base64
import binascii

def hex2base64(bytestr):
    return base64.b64encode(bytestr)

if __name__ == "__main__":
    inp = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d"
    # convert string to raw bytes
    binary = binascii.unhexlify(inp)
    print(binary)
    print(hex2base64(binary))