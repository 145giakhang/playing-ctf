import binascii
import string

CHARS = string.printable

if __name__ == "__main__":
    inp = "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736"
    # convert to raw bytes
    raw = binascii.unhexlify(inp)

    # for each possible ASCII character, XOR input string 
    lst_chars = []
    lst_strs = []
    for c in CHARS:
        raw_c = ord(c)
        lst_chars.append(c)
        lst_strs.append(''.join([chr(raw_c^b) for b in raw]))

    # find the most English-like string
    with open('words.txt', 'r') as fp:
        dictionary = fp.read().strip().split('\n')
        for i in range(len(lst_strs)):
            score = 0
            lst_words = lst_strs[i].split()
            for word in lst_words:
                if word in dictionary:
                    score += 1

            if (score * 100) / len(lst_words) >= 60:
                print("{}: {}".format(lst_chars[i], lst_strs[i]))

# X: Cooking MC's like a pound of bacon

    
                    



    