from Crypto.Cipher import AES
import base64

key = "YELLOW SUBMARINE"

def aes_decrypt(key, my_msg):
    aes = AES.new(key, AES.MODE_ECB)
    # decode from base64 to raw bytes
    base64_decrypted = base64.decodebytes(my_msg.encode(encoding='utf-8'))
    # decrypted to plaintext str
    decrypted_text = str(aes.decrypt(base64_decrypted), encoding='utf-8')
    return decrypted_text

with open('7.txt', 'r') as fp:
    msg = ''.join(fp.read().strip().split('\n'))
    print(aes_decrypt(key, msg))
