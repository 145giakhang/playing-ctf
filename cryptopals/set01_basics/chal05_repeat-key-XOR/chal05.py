import binascii

def encrypt(plaintxt, key="ICE"):
    # get raw key
    raw_key = bytearray.fromhex(binascii.hexlify(binascii.a2b_qp(key)).decode("utf-8"))
    res = bytearray(len(plaintxt))
    for i in range(len(plaintxt)):
        res[i] = plaintxt[i] ^ raw_key[i%len(key)]

    return res.hex()

if __name__ == '__main__':
    # read plaintext exclude new-line at last
    plaintxt = open("plaintext.txt").read()[:-1]
    # plaintext string to binary format
    binstr = binascii.a2b_qp(plaintxt)
    print(binstr)
    # hexadecimal present of binary data
    hexstr = binascii.hexlify(binstr)
    print(hexstr.decode("utf-8"))
    raw = bytearray.fromhex(hexstr.decode("utf-8"))
    print(raw)
    print(encrypt(raw))
