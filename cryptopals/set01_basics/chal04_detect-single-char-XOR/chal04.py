import binascii
import string

CHARS = string.printable

if __name__ == "__main__":
	with open('4.txt', 'r') as fp1:
		line = fp1.readline().strip()
		while line:
			# convert to raw bytes
			raw = binascii.unhexlify(line)

			# for each possible ASCII character, XOR input string 
			lst_chars = []
			lst_strs = []
			for c in CHARS:
				raw_c = ord(c)
				lst_chars.append(c)
				lst_strs.append(''.join([chr(raw_c^b) for b in raw]))

			# find the most English-like string
			with open('words.txt', 'r') as fp2:
				dictionary = fp2.read().strip().split('\n')
				for i in range(len(lst_strs)):
					score = 0
					lst_words = lst_strs[i].split()
					for word in lst_words:
						if word in dictionary:
							score += 1

					if (score * 100) / len(lst_words) >= 60:
						print("{}: {}".format(lst_chars[i], lst_strs[i]))

			line = fp1.readline().strip()

# 5: Now that the party is jumping