from collections import defaultdict
import binascii

def total_repeat(text, blocksz = 16):
    # decode from hex to raw bytes
    raw = binascii.unhexlify(text)

    reps = defaultdict(lambda: -1)
    for i in range(0, len(raw), blocksz):
        reps[raw[i:i+blocksz]] += 1
    return sum(reps.values())

if __name__ == "__main__":
    with open('8.txt', 'r') as fp:
        line = fp.readline().strip()
        max_reps = 0
        ecb_ciphertxt = None
        while line:
            reps = total_repeat(line)
            if reps > max_reps:
                max_reps = reps
                ecb_ciphertxt = line

            line = fp.readline().strip()

        print(ecb_ciphertxt)
