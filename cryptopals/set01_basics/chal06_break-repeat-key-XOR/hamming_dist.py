import binascii

def hamming_dist(s1, s2):
    '''
    Input: 2 raw strings with same length
    Output: number of difference between 2 strings in bit
    '''
    slen = len(s1)
    if slen != len(s2):
        print("Hamming distance: 2 strings must be same lenght.")
        return -1

    dist = 0
    
    # XOR 2 raw bytes together,
    # count number of ocurrence of 1's bit => number of difference
    for i in range(slen):
        xor = s1[i] ^ s2[i]
        dist += bin(xor).count("1")

    return dist

if __name__ == '__main__':
    str1 = "this is a test"
    str2 = "wokka wokka!!!"
    # should be 37
    print(hamming_dist(binascii.unhexlify(str1.encode('utf-8').hex()), binascii.unhexlify(str2.encode('utf-8').hex())))
