import hamming_dist as hd
import binascii
import base64
import string

CHARS = string.printable 


def get_score(inp_chars):
    character_frequencies = {
        'a': .08167, 'b': .01492, 'c': .02782, 'd': .04253,
        'e': .12702, 'f': .02228, 'g': .02015, 'h': .06094,
        'i': .06094, 'j': .00153, 'k': .00772, 'l': .04025,
        'm': .02406, 'n': .06749, 'o': .07507, 'p': .01929,
        'q': .00095, 'r': .05987, 's': .06327, 't': .09056,
        'u': .02758, 'v': .00978, 'w': .02360, 'x': .00150,
        'y': .01974, 'z': .00074, ' ': .13000
    }
    return sum([character_frequencies.get(char, 0) for char in inp_chars.lower()])


def single_byte_xor(inp):
    lst_chars = []
    lst_strs = []
    for c in CHARS:
        raw_c = ord(c)
        lst_chars.append(c)
        lst_strs.append(''.join([chr(raw_c^b) for b in inp]))
    return zip(lst_chars, lst_strs)


def transpose(blocks):
    '''
    Input: a list of raw byte strings
    Output: transpose these strings => return a list of lists, each contains:
        + first bytes of all strings
        + second bytes of all strings
        + so on...
    '''
    transpose_blocks = []
    for i in range(len(blocks[0])):
        s = []
        for b in blocks:
            if i < len(b):
                s.append(b[i])
        transpose_blocks.append(s)
    return transpose_blocks


def divide(ciphertext, keysize):
    '''
    Input: ciphertext in bytes, keysize
    Output: a list of keysize blocks from ciphertext
    '''
    blocks = []
    textlen = len(ciphertext)
    num_of_full_size_blocks = textlen // keysize 
    for i in range(num_of_full_size_blocks):
        blocks.append(ciphertext[keysize*i:keysize*(i+1)])
   
    # take the last "non-full keysize" block
    if textlen % keysize != 0:
        blocks.append(ciphertext[num_of_full_size_blocks * keysize:])

    return blocks


def find_keysize(ciphertext):
    # list of tuples (<keysize>, <normalized distance vlaue>)
    lst = []
    # test keysize from length 2 to 40
    for keysz in range(2, 41):
        block1 = ciphertext[:keysz]
        block2 = ciphertext[keysz:2*keysz]
        block3 = ciphertext[2*keysz:3*keysz]
        block4 = ciphertext[3*keysz:4*keysz]

        normalized_dist = (hd.hamming_dist(block1, block2)
                          + hd.hamming_dist(block2, block3)
                          + hd.hamming_dist(block3, block4)) / (keysz * 3)
        lst.append((keysz, normalized_dist))

    # sort in ascending order of edit distance
    lst.sort(key = lambda x: x[1])
    # take 3 smallest
    return lst[:3]

if __name__ == '__main__':
    try:
        fp = open('6.txt', 'r')
        # decode to ciphertext
        ciphertext = base64.b64decode(''.join(fp.read().strip().split('\n')))
        # find possible key-size
        lst_keysize = find_keysize(ciphertext)
        # loop for each probally keysize
        for KEYSIZE, _ in lst_keysize:
            # divide ciphertext into blocks of keysize
            blocks_of_keysize = divide(ciphertext, KEYSIZE)
            # transpose
            trans = transpose(blocks_of_keysize)
            print("======== Key-size = {} ========".format(str(KEYSIZE)))
            # for each transpose block, single-byte XOR
            possible_key = []
            for tb in trans:
                key_str_pairs = single_byte_xor(tb)
                # for each pair of single char key + decoded str,
                # returns the score of a message based on the
                # relative frequency the characters occur in the English language,
                # then get the best looking histogram
                max_score = 0.0
                best = None
                for pair in tuple(key_str_pairs):
                    score = get_score(pair[1])
                    if score > max_score:
                        max_score = score
                        best = pair
                possible_key.append(best[0]) 
            
            # print out the key "Terminator X: Bring the noise"
            key = ''.join(possible_key)
            print("=== Key: {}".format(key))
            # now let's decrypt the ciphertext
            keystr = key * (len(ciphertext) // KEYSIZE) + key[:(len(ciphertext) % KEYSIZE)]
            plaintext = ''.join([chr(tup[0] ^ ord(tup[1])) for tup in list(zip(ciphertext, keystr))])
            print("=== Plaintext:\n{}".format(plaintext))
    except IOError as err:
        print("Error: " + str(err))
    finally:
        fp.close()
