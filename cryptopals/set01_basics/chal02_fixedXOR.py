import binascii

def fixedXor(s):
    fixed_raw_str = binascii.unhexlify("686974207468652062756c6c277320657965")
    # convert input string to raw bytes
    raw_input = binascii.unhexlify(s)
    lst = [x[0]^x[1] for x in zip(raw_input, fixed_raw_str)]
    # return raw bytes, of course
    return binascii.hexlify(bytearray(lst))


if __name__ == '__main__':
    hexstr = "1c0111001f010100061a024b53535009181c"
    raw = fixedXor(hexstr)
    print("Raw hex str: {}".format(raw))
    # print(binascii.hexlify(raw))